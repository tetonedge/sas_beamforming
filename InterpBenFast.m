function Yi = InterpBenFast(X, Y, Xi)

% this is extremly fast table lookup. It assumes that the X values are
% monotomicly increasing and it only does linear interpolation. X and Y
% must be column vectors

DX = X(2) - X(1);
Ilo = floor((Xi - X(1))./DX) + 1;
Ihi = ceil((Xi - X(1))./DX) + 1;

for J = 1:length(Xi)
    if ~isreal(Xi(J)) | ~isfinite(Xi(J))
        Yi(J) = 0;
    elseif Ilo(J) < 1
        Yi(J) = Y(1);
    elseif Ihi(J) > length(X)
        Yi(J) = Y(end);
    elseif Ilo(J) == Ihi(J)
        Yi(J) = Y(Ilo(J));
    else
        M = (Y(Ihi(J)) - Y(Ilo(J)))./(DX);
        B = Y(Ilo(J)) - M.*X(Ilo(J));
        Yi(J) = M.*Xi(J) + B;
    end
end

Sz = size(Yi);
if Sz(2) >1
    Yi = Yi.';
end
