function ImagePlane = FlatImagePlane(Xrange, Yrange, Z0, Res)

% All measurements in meters
% Xrange -> [Xstart Xend] direction of returns  
% Yrange -> [Ystart Yend] path of the vehicle
% Z0 -> hight of plane
% Res -> distance between points
% SASres -> Relative range resolution returns (1 means that the SAS
%     resolution is the same as Res.

Xv = [Xrange(1):Res:Xrange(2)];
Yv = [Yrange(1):Res:Yrange(2)];

[ImagePlane.X ImagePlane.Y] = meshgrid(Xv, Yv);
ImagePlane.Z = Z0.*ones(size(ImagePlane.X));

ImagePlane.Resolution = Res;


imagesc(Xv, Yv, ImagePlane.Y)
set(gca, 'DataAspectRatio', [1 1 1])