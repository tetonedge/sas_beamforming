 function [WaveOutProcessed, Image] = BensSASProcessor(WaveformOut, WaveformIn, Transducer, ImagePlane, Fluid, PlotOpt)

%function [WaveOutProcessed] = BensSASProcessor(WaveformOut, WaveformIn, Transducer, ImagePlane, Fluid)
% This is Ben's SAS processor
% inupts:
% (transducer position index is I)
%
% WaveformOut.Data -> vector of data for the outgoing pulse
% WaveformOut.T0 -> start time of outgoing pulse (usually 0)
% WaveformOut.SampFreq -> sample frequency of outgoing pulse
%
% WaveformIn.Data -> matrix of incomming data [Data Points, I]
% WaveformIn.T0 -> start time of return signals
% WaveformIn.SampFreq -> sample frequency of incomming data
%
% Transducer.SdPosition -> Send Transducer position matrix Ix3 [X(I) Y(I) Z(I)]
% Transducer.SdDirection -> unit vectors of orientation of send transducer at each position Ix3 [Dx(I) Dy(I) Dz(I)]
% Transducer.RePosition -> Send Transducer position matrix Ix3 [X(I) Y(I) Z(I)]
% (There is no ReDirection as the reciever is assumed to be omnidirectional or folded into the shading)
% Transducer.ShadingH -> vector of values to shade the response by in the vertical direction
% Transducer.ShadingV -> vector of values to shade the response by in the horizontal direction (if .ShadingV is empty, shading is spherical and .ShadingH contains the values
% Transducer.ShadingAngle -> vector of angles corresponding to .ShadingH and .ShadingV
%
% ImagePlane.X -> Image plane in the spirit of Matlab's 3-D surf plotting
% ImagePlane.Y -> Image plane in the spirit of Matlab's 3-D surf plotting
% ImagePlane.Z -> Altitude of Image plane, same size as .X and .Y
% ImagePlane.Resolution -> lenght of of area (in space) overwhich data for each cell is taken (should be on the order of the cell size)
%
% Fluid.C -> Sound speed in surrounding Fluid (1481 m/s if left as an empty matrix)
% Fluid.Alpha -> Attenuation in surrounding Fluid (0 Np/m if left as an empty matrix)
%
% PlotOpt -> Plotting of various stages
%   PlotOpt == 1, plot signal processing info and stop
%   PlotOpt == 2, plot distances and angles for a single point (see the thrid line of the Beamforming cell)

%% Default values
if isempty(Fluid)
    Fluid.C = 1481;
    Fluid.Alpha = 0;
end
if all(size(Transducer.ShadingV)) == all(size(Transducer.ShadingH))
    ShadeMark = 0; % shading is horizontal and vertical
elseif isempty(Transducer.ShadingV)
    ShadeMark = 1; % shading in circular
else
    error('Vertical shading must be the same size as horizontal or Vertical can be empty for circular shading')
end

%% Some fundimental constants
N = size(Transducer.RePosition, 1); % number of transducer positions.
LenIn = size(WaveformIn.Data, 2); % length of incoming pulse
LenOut = length(WaveformOut.Data); % length of outgoing pulse
Fs = WaveformIn.SampFreq; % sample frequency
Dt = 1./Fs; % Time step size
Dx = Fluid.C./Fs; % Space step size

%% Inupt checking
if WaveformOut.T0 ~= 0 % time shift so the outgoing pulse is at 0
    WaveformIn.T0 = WaveformIn.T0 - WaveformOut.T0;
    WaveformOut.T0 = 0;
end
if size(WaveformIn.Data, 1) ~= N
    error('The number of transducer positions must match the data samples taken')
end
if WaveformOut.SampFreq ~= Fs
    error('The sample frequency of the outgoing and incomming waves must match')
end
if size(Transducer.SdPosition, 1) ~= N || size(Transducer.SdDirection, 1) ~= N || size(Transducer.RePosition, 1) ~= N
    error('The size of the Transducer positions and/or directions must match')
end

%% Initial Vectors
Rvector = WaveformIn.T0.*Fluid.C + [0:(LenIn - 1)].*Dx; % range vector, distances correspoding to each data point.
Atten0 = exp(Rvector.*Fluid.Alpha).*Rvector; % Corrects for attenuation in the fluid and 1/R spreading
% RvectorIx = 1:length(Rvector); %Indexes of Rvector, for interpolation
% RIxRange = [1 LenIn];  % minimum and maximum distances in index. Outside this range there is no data.

%% Make a filter
NFilt  = 4;   % Order
Fc = 10e6;  % Cutoff Frequency
% Calculate the A & B values using the BUTTER function.
[B, A] = butter(NFilt, Fc/(Fs/2));
Pow = nextpow2(size(WaveformIn.Data, 2));
%WaveformOut.Data = filtfilt(B, A, WaveformOut.Data);

%% Initial Signal Processing
disp('Signal Processing')

for I = 1:N
    Temp = WaveformIn.Data(I, :) - sum(WaveformIn.Data(I, :))./LenIn; %zero mean
    Temp = filtfilt(B, A, Temp); %Filter to 10 MHz or so
    %Temp = conv(Temp, WaveformOut.Data); %convolve with outgoing pulse
    %Temp = Temp(1:end - LenOut + 1); %truncate
    WaveformIn.Data(I, :) = Temp.*Atten0;
end
if PlotOpt == 1
    Time = WaveformIn.T0 + Dt.*(1:LenIn);
    figure('Position', [360 102 560 820])
    subplot(2, 1, 1)
    imagesc(1:N, Time, WaveformIn.Data)
    Ax1 = gca;
    title('Raw Data')
    subplot(2, 1, 2)
    imagesc(1:N, Time, WaveformIn.Data)
    Ax2 = gca;
    linkaxes([Ax1 Ax2])
    title('Signal Processed Data')
    figure
    plot(WaveformIn.Data(round(N/2), :))
    
%     Freq = linspace(0, Fs);
%     FFTRaw = fft(WaveformIn.Data, [], 1);
%     FFTProc = fft(Temp2, [], 1);
%     figure('Position', [360 102 560 820])
%     subplot(2, 1, 1)
%     imagesc(1:N, Freq, abs(FFTRaw))
%     Ax1 = gca;
%     title('FFT of Raw Data')
%     subplot(2, 1, 2)
%     imagesc(1:N, Freq, abs(FFTProc))
%     Ax2 = gca;
%     linkaxes([Ax1 Ax2])
%     title('FFT of Signal Processed Data (before Hilbert T')
    
    disp('Terminating Program (PlotOpt == 1 is only concerned with the signal processing.)')
    WaveOutProcessed = WaveformIn.Data;
    Image = 0;
    return
end
clear Temp2

%% Beamforming
disp('Beamforming')
Image = zeros(size(ImagePlane.X));
if PlotOpt == 2
    Is = [100 100];
    Js = [100 100];
else
    Is = [1 size(ImagePlane.X, 1)];
    Js = [1 size(ImagePlane.Y, 2)];
end

Amplitudes = zeros(1, N);
for I = Is(1):Is(2) % stepping through x-position
    tic
    for J = Js(1):Js(2); % stepping through Y-position
        PlanePt = [ImagePlane.X(I, J) ImagePlane.Y(I, J) ImagePlane.Z(I, J)];
        R = Dist(PlanePt, Transducer.SdPosition) + Dist(PlanePt, Transducer.RePosition); % round trip distance in meters
        Rindex = round((R - WaveformIn.T0.*Fluid.C)./Dx - 1); % round trip distance in index
        if ShadeMark == 0
            ThetaV = ThetaVCalc(PlanePt, Transducer.SdPosition, Transducer.SdDirection);
            ThetaH = ThetaHCalc(PlanePt, Transducer.SdPosition, Transducer.SdDirection);
            AttenV = InterpBenFast(Transducer.ShadingAngle, Transducer.ShadingV, ThetaV);
            AttenH = InterpBenFast(Transducer.ShadingAngle, Transducer.ShadingH, ThetaH);
        elseif ShadeMark == 1
            ThetaCirc = ThetaCircCalc(PlanePt, Transducer.SdPosition, Transducer.SdDirection);
            AttenV = 0;
            AttenH = InterpBenFast(Transducer.ShadingAngle, Transducer.ShadingH, ThetaCirc);
        end
        if PlotOpt == 2
            % plane pt - 1
            PlanePt = [ImagePlane.X(I-1, J) ImagePlane.Y(I-1, J) ImagePlane.Z(I-1, J)];
            R = Dist(PlanePt, Transducer.SdPosition) + Dist(PlanePt, Transducer.RePosition); % round trip distance in meters
            RindexM1 = round((R - WaveformIn.T0.*Fluid.C)./Dx - 1); % round trip distance in index
            
            figure
            imagesc(log(abs(WaveformIn.Data)))
            hold on
            plot(Rindex)
            plot(RindexM1)
            title('Round Trip distance')
            figure
            if exist('ThetaV', 'var')
                plot(ThetaV)
                hold on
                title('Vertical (blue) and Horizontal (red) angles')
            plot(ThetaH)
            else
                plot(ThetaCirc)
                title('Circular angles')
            end

        end
        % Group Attenuations (Shading)
        if ShadeMark == 0
            Atten = AttenV.*AttenH;
        else
            Atten = AttenH;
        end
        % Get the Amplitudes
        NoIncluded = 0; % number of included data points
        for K = 1:N
            if Atten(K) ~= 0
                Amp0 = InterpBen4(Rvector, WaveformIn.Data(K, :), R(K));
                Amplitudes(K) = Atten(K).*Amp0;
                NoIncluded = NoIncluded + 1;
            end
        end
        if PlotOpt == 2
            figure
            plot(Amplitudes)
            title('Amplitudes')
        end
        Image(I, J) = abs(sum(Amplitudes))./NoIncluded;
    end
    disp([num2str(I) ' of ' num2str(Is(2)) ', Time Remaining = ' num2str(toc*(Is(2) - I)) ' s'])
end
if PlotOpt == 3
    figure
    imagesc(ImagePlane.X(1, :), ImagePlane.Y(:, 1), Image)
    set(gca, 'YDir', 'normal', 'DataAspectRatio', [1 1 1])
    xlabel('X (m)')
    ylabel('Y (m)')
end

%% fixing output
WaveOutProcessed = WaveformIn.Data;

%% Saving output
save TempSASResults WaveformOut WaveformIn Transducer ImagePlane Fluid WaveOutProcessed Image

%% subfunctions
function D = Dist(X1, X2)
% the distances when X1 is a single position vector and X2 is a column of
% position vectors. Returns a column of distances
X1 = repmat(X1, size(X2, 1), 1);
D = sqrt(sum((X1 - X2).^2, 2));

function ThetaV = ThetaVCalc(Ip, Tp, Td)
% Calculates ThetaV for Image position, Ip, Transducer position Tp (A column
% of position vectors), and Td (A column of unit directional vectors)
Ip = repmat(Ip, size(Tp, 1), 1);
ThetaV = atan2(Td(:, 1), Td(:, 3)) - atan2(Ip(:, 1) - Tp(:, 1), Ip(:, 3) - Tp(:, 3));

function ThetaH = ThetaHCalc(Ip, Tp, Td)
% Calculates ThetaH for Image position, Ip, Transducer position Tp (A column
% of position vectors), and Td (A column of unit directional vectors)
Ip = repmat(Ip, size(Tp, 1), 1);
ThetaH = atan2(Td(:, 2), Td(:, 1)) - atan2(Ip(:, 2) - Tp(:, 2), Ip(:, 1) - Tp(:, 1));

function ThetaCirc = ThetaCircCalc(Ip, Tp, Td)
% Calculates ThetaCirc for Image position, Ip, Transducer position Tp (A column
% of position vectors), and Td (A column of unit directional vectors). This
% is for a transducer with circular shading.
Ip = repmat(Ip, size(Tp, 1), 1);
B = Ip - Tp;
% use definition of dot product
AbsA = sqrt(Td(:, 1).^2 + Td(:, 2).^2 + Td(:, 3).^2);
AbsB = sqrt(B(:, 1).^2 + B(:, 2).^2 + B(:, 3).^2);
AdotB = Td(:, 1).*B(:, 1) + Td(:, 2).*B(:, 2) + Td(:, 3).*B(:, 3);
ThetaCirc = acos(AdotB./(AbsA.*AbsB));