function Yi = InterpBen4(X, Y, Xi)

% This is an extremly fast interpolation function. X must be monotoniclly
% increasing! and Xi must be a scalar.
%
% This method uses the legrange basis to invert the Vandermonde Matrix. 

if numel(Xi) ~= 1
    error('For InterpBen4, Xi must be a scalar')
end

% find the nearest four points
Dx = (X(2) - X(1));
Ix0 = floor((Xi - X(1))./Dx);

% if near the ends, just do linear.
if Ix0 < 1
    M = (Y(2) - Y(1))/(X(2) - X(1));
    B = Y(1) - M*X(1);
    Yi = M*Xi + B;
    return
elseif Ix0 > numel(X) - 3
    M = (Y(end) - Y(end - 1))/(X(end) - X(end - 1));
    B = Y(end - 1) - M*X(end - 1);
    Yi = M*Xi + B;
    return
end

% this is extremly fast cubic interpolation
Dx3 = Dx^3;
L1 = (1/(-6*Dx3)).*(Xi - X(Ix0 + 1)).*(Xi - X(Ix0 + 2)).*(Xi - X(Ix0 + 3));
L2 = (1/(2*Dx3)).*(Xi - X(Ix0)).*(Xi - X(Ix0 + 2)).*(Xi - X(Ix0 + 3));
L3 = (1/(-2*Dx3)).*(Xi - X(Ix0)).*(Xi - X(Ix0 + 1)).*(Xi - X(Ix0 + 3));
L4 = (1/(6*Dx3)).*(Xi - X(Ix0)).*(Xi - X(Ix0 + 1)).*(Xi - X(Ix0 + 2));

Yi = Y(Ix0).*L1 + Y(Ix0 + 1).*L2 + Y(Ix0 + 2).*L3 + Y(Ix0 + 3).*L4;
